type FunctionType = {
  /** 功能名称 */
  name: string;
  /** 执行方法 */
  fn: (...args: unknown[]) => void | (() => void);
}

/**
 * 聊天室类别定义
 */
declare class LgChatRoomType<Message = unknown> {
  /** 聊天室id */
  chatId?: string | number;
  /** 我的id */
  myId?: number;
  /** 聊天室建立的时间戳 */
  createTime?: number
  /** 功能列表 */
  functions: FunctionType[];
  /** 发送者 */
  sender?: LgChatRoomSenderType;
  /** 讯息变更处理者 */
  msgHandlers: Map<'add_history' | 'add_new', Set<(...args: unknown[]) => void>>;
  /** 历史纪录讯息列表 */
  historyMessages: Message[];
  /** 新讯息列表 */
  newMessages: Message[];

  /**
   * 初始化
   * @param chatId 聊天室id
   * @param myId 我的id
   * @param beforeInitFn 初始化前的回调方法
   * @param afterInitFn 初始化后的回调方法
   */
  init: (chatId: number | string, myId: number, historyMessages?: Message[]) => void;

  /**
   * 销毁聊天室
   */
  destroy: () => void;

  /**
   * 取得所有聊天讯息
   */
  getAllMessage: () => Message[];

  /**
   * 取得历史纪录的聊天讯息
   */
  getHistoryMessages: () => Message[];
  /**
   * 设定历史纪录的聊天讯息
   */
  appendHistoryMessages: (list: Message[]) => void;

  /**
   * 取得新的聊天讯息(非历史纪录)
   */
  getNewMessages: () => Message[];
  /**
    * 设定新的聊天讯息(非历史纪录)
    */
  appendNewMessages: (list: Message[]) => void;

  /**
   * 聊天室状态
   * @returns {{
   *  isBlock: Boolean, // 是否block
   *  info?: any // block 信息
   * }}
   */
  getStatus: () => {
    isBlock: boolean,
    info?: unknown
  };

  /**
   * 设定功能
   * @param functions 功能列表
   */
  setupFunctions: (functions: FunctionType[]) => void;

  /**
   * 执行功能
   * @param name 功能名称
   * @param args 传入参数
   */
  callFunction: (name: string, ...args: unknown[]) => Promise<unknown> | unknown;

  /**
   * 新增订阅讯息变更的处理方法
   * @param key 变更的Key
   * @param fn 讯息变更要执行方法
   */
  subscribe: (key: 'add_history' | 'add_new', fn: (...args: unknown[]) => void) => void;

  /**
   * 解除订阅讯息变更的处理方法
   * @param key 变更的Key
   * @param fn 讯息变更要执行方法
   */
  unsubscribe: (key: 'add_history' | 'add_new', fn: (...args: unknown[]) => void) => void;

  /**
   * 执行特定讯息变更的所有处理方法
   * @param key 变更的Key
   * @param args 传入参数
   */
  triggerMsgHandlers: (key: 'add_history' | 'add_new', ...args: unknown[]) => void;
}

/**
 * 发送者
 */
declare class LgChatRoomSenderType {
  /** 功能列表 */
  readonly functions: FunctionType[];

  /**
   * 传送讯息
   * @param chatroom
   */
  sendMessage: (chatroom: LgChatRoomType) => Promise<unknown>;

  /**
   * 设定功能
   * @param functions 功能列表
   */
  setupFunctions: (functions: FunctionType[]) => void;

  /**
   * 执行功能
   * @param name 功能名称
   * @param args 传入参数
   */
  callFunction: (name: string, ...args: unknown[]) => Promise<unknown> | unknown;

  /**
   * 销毁发送者
   */
  destroy: () => void;
}

declare const LgChat: {
  LgChatRoomSender: new () => LgChatRoomSenderType;
  LgChatRoom: new () => LgChatRoomType;
}

export { type FunctionType, LgChatRoomSenderType, LgChatRoomType, LgChat as default };
