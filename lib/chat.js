(function (global, factory) {
  typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports) :
  typeof define === 'function' && define.amd ? define(['exports'], factory) :
  (global = typeof globalThis !== 'undefined' ? globalThis : global || self, factory(global["lg-chat"] = {}));
})(this, (function (exports) { 'use strict';

  function _classCallCheck(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }
  function _defineProperties(target, props) {
    for (var i = 0; i < props.length; i++) {
      var descriptor = props[i];
      descriptor.enumerable = descriptor.enumerable || false;
      descriptor.configurable = true;
      if ("value" in descriptor) descriptor.writable = true;
      Object.defineProperty(target, _toPropertyKey(descriptor.key), descriptor);
    }
  }
  function _createClass(Constructor, protoProps, staticProps) {
    if (protoProps) _defineProperties(Constructor.prototype, protoProps);
    if (staticProps) _defineProperties(Constructor, staticProps);
    Object.defineProperty(Constructor, "prototype", {
      writable: false
    });
    return Constructor;
  }
  function _defineProperty(obj, key, value) {
    key = _toPropertyKey(key);
    if (key in obj) {
      Object.defineProperty(obj, key, {
        value: value,
        enumerable: true,
        configurable: true,
        writable: true
      });
    } else {
      obj[key] = value;
    }
    return obj;
  }
  function _toConsumableArray(arr) {
    return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread();
  }
  function _arrayWithoutHoles(arr) {
    if (Array.isArray(arr)) return _arrayLikeToArray(arr);
  }
  function _iterableToArray(iter) {
    if (typeof Symbol !== "undefined" && iter[Symbol.iterator] != null || iter["@@iterator"] != null) return Array.from(iter);
  }
  function _unsupportedIterableToArray(o, minLen) {
    if (!o) return;
    if (typeof o === "string") return _arrayLikeToArray(o, minLen);
    var n = Object.prototype.toString.call(o).slice(8, -1);
    if (n === "Object" && o.constructor) n = o.constructor.name;
    if (n === "Map" || n === "Set") return Array.from(o);
    if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen);
  }
  function _arrayLikeToArray(arr, len) {
    if (len == null || len > arr.length) len = arr.length;
    for (var i = 0, arr2 = new Array(len); i < len; i++) arr2[i] = arr[i];
    return arr2;
  }
  function _nonIterableSpread() {
    throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.");
  }
  function _toPrimitive(input, hint) {
    if (typeof input !== "object" || input === null) return input;
    var prim = input[Symbol.toPrimitive];
    if (prim !== undefined) {
      var res = prim.call(input, hint || "default");
      if (typeof res !== "object") return res;
      throw new TypeError("@@toPrimitive must return a primitive value.");
    }
    return (hint === "string" ? String : Number)(input);
  }
  function _toPropertyKey(arg) {
    var key = _toPrimitive(arg, "string");
    return typeof key === "symbol" ? key : String(key);
  }

  var LgChatRoomSender = /*#__PURE__*/_createClass(function LgChatRoomSender() {
    var _this = this;
    _classCallCheck(this, LgChatRoomSender);
    _defineProperty(this, "functions", []);
    _defineProperty(this, "sendMessage", function (chatroom) {
      console.warn('Method not implemented.');
      ({
        chatId: chatroom.chatId,
        myId: chatroom.myId,
        msg: "this is test message."
      });
      return new Promise(function (resolve, reject) {
        window.setTimeout(function () {
          var response = {
            code: 200,
            message: 'success',
            data: {
              messageId: 123
            }
          };
          resolve(response.data);
        }, 300);
      });
    });
    _defineProperty(this, "setupFunctions", function (functions) {
      console.warn('Method not implemented.');
      _this.functions = functions;
    });
    _defineProperty(this, "callFunction", function (name) {
      // console.warn('Method not implemented.');
      var fnObj = _this.functions.find(function (fn) {
        return fn.name === name;
      });
      for (var _len = arguments.length, args = new Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
        args[_key - 1] = arguments[_key];
      }
      var result = fnObj === null || fnObj === void 0 ? void 0 : fnObj.fn.apply(fnObj, args);
      if (result && result instanceof Promise) {
        return Promise.resolve(result);
      } else {
        return result;
      }
    });
    _defineProperty(this, "destroy", function () {
      console.warn('Method not implemented.');
      _this.functions = [];
    });
  });

  /**
   * 聊天室类别
   */
  var LgChatRoom = /*#__PURE__*/_createClass(function LgChatRoom() {
    var _this = this;
    _classCallCheck(this, LgChatRoom);
    _defineProperty(this, "functions", []);
    _defineProperty(this, "msgHandlers", new Map());
    _defineProperty(this, "historyMessages", []);
    _defineProperty(this, "newMessages", []);
    _defineProperty(this, "init", function (chatId, myId, historyMessages) {
      console.warn('Method not implemented.');
      _this._init(chatId, myId);
      if (historyMessages) {
        _this.appendHistoryMessages(historyMessages);
      }
    });
    _defineProperty(this, "destroy", function () {
      console.warn('Method not implemented.');
      _this.chatId = undefined;
      _this.myId = undefined;
      _this.createTime = undefined;
    });
    _defineProperty(this, "getAllMessage", function () {
      //console.warn('Method not implemented.');
      return [].concat(_this.getHistoryMessages(), _this.getNewMessages());
    });
    /**
     * 取得历史纪录的聊天讯息
     */
    _defineProperty(this, "getHistoryMessages", function () {
      // console.warn('Method not implemented.');
      return _this.historyMessages;
    });
    _defineProperty(this, "appendHistoryMessages", function (list) {
      var _this$historyMessages;
      //console.warn('Method not implemented.');
      (_this$historyMessages = _this.historyMessages).push.apply(_this$historyMessages, _toConsumableArray(list));
      _this.triggerMsgHandlers('add_history');
    });
    /**
     * 取得新的聊天讯息(非历史纪录)
     */
    _defineProperty(this, "getNewMessages", function () {
      // console.warn('Method not implemented.');
      return _this.newMessages;
    });
    _defineProperty(this, "appendNewMessages", function (list) {
      var _this$newMessages;
      //console.warn('Method not implemented.');
      (_this$newMessages = _this.newMessages).push.apply(_this$newMessages, _toConsumableArray(list));
      _this.triggerMsgHandlers('add_new');
    });
    _defineProperty(this, "getStatus", function () {
      console.warn('Method not implemented.');
      return {
        isBlock: false
      };
    });
    _defineProperty(this, "setupFunctions", function (functions) {
      // console.warn('Method not implemented.');
      _this.functions = functions;
    });
    _defineProperty(this, "callFunction", function (name) {
      // console.warn('Method not implemented.');
      var fnObj = _this.functions.find(function (fn) {
        return fn.name === name;
      });
      for (var _len = arguments.length, args = new Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
        args[_key - 1] = arguments[_key];
      }
      var result = fnObj === null || fnObj === void 0 ? void 0 : fnObj.fn.apply(fnObj, args);
      if (result && result instanceof Promise) {
        return Promise.resolve(result);
      } else {
        return result;
      }
    });
    _defineProperty(this, "subscribe", function (key, fn) {
      if (!_this.msgHandlers.has(key)) {
        _this.msgHandlers.set(key, new Set());
      }
      var fns = _this.msgHandlers.get(key);
      fns === null || fns === void 0 || fns.add(fn);
    });
    _defineProperty(this, "triggerMsgHandlers", function (key) {
      for (var _len2 = arguments.length, args = new Array(_len2 > 1 ? _len2 - 1 : 0), _key2 = 1; _key2 < _len2; _key2++) {
        args[_key2 - 1] = arguments[_key2];
      }
      var fns = _this.msgHandlers.get(key);
      fns === null || fns === void 0 || fns.forEach(function (fn) {
        return fn.apply(void 0, [_this].concat(args));
      });
    });
    _defineProperty(this, "unsubscribe", function (key, fn) {
      var set = _this.msgHandlers.get(key);
      set === null || set === void 0 || set["delete"](fn);
    });
    /** private method */
    _defineProperty(this, "_init", function (chatId, myId) {
      _this.chatId = chatId;
      _this.myId = myId;
      _this.createTime = Date.now();
      _this.historyMessages = [];
      _this.newMessages = [];
      _this.sender = new LgChatRoomSender();
    });
  });

  exports.LgChatRoom = LgChatRoom;

}));
