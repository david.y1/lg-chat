import path from 'node:path';
import babel from 'rollup-plugin-babel';
import nodeResolve from '@rollup/plugin-node-resolve';
import { dts } from 'rollup-plugin-dts';

const pkg = require('./package.json');

const extensions = ['.js', '.ts'];

const resolve = function (...args) {
  return path.resolve(__dirname, ...args);
}
// console.log('env', process.env.FORMAT);
const config = [
  {
    input: resolve('./src/index.ts'),
    output: {
      name: 'lg-chat',
      file: resolve('./', pkg.main),
      format: process.env.FORMAT || 'umd',
    },
    plugins: [
      nodeResolve({
        extensions,
        modulesOnly: true,
      }),
      babel({
        exclude: 'node_modules/**',
        extensions
      }),
    ]
  },
  {
    input: resolve('./types/index.d.ts'),
    output: [{ file: "lib/chat.d.ts", format: "es" }],
    plugins: [dts()],
  },
]

// ref1: https://segmentfault.com/a/1190000021695864
// ref2: https://github.com/garycourt/uri-js/blob/master/package.json

export default config;
