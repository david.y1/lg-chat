# lg-Chat

This is private module class that let you extend the chat class.

# NodeJS

v14.21.x up.

# How to build module

1. `npm install` must be run before building.
2. run cmd `npm run build` to generate bundle javascript that format is `umd` module

# How to publish

1. update `package.json` version
2. commit and push to remote gitlab
3. add tag into gitlab repo. eg: `v1.0.2`

# How to Install this module

1. Copy this `git+http://10.45.110.107:8888/david/lg-chat.git#v1.0.2` into your repo package `dependencies` block with `version tag`,
   or run cmd `npm install git+http://10.45.110.107:8888/david/lg-chat.git#v1.0.2` install this module.

    eg:
    ```json
    // package.json
    {
      "name": "xxx",
      ...
      "dependencies": {
        "lg-chat": "git+http://10.45.110.107:8888/david/lg-chat.git#v1.0.2", // 'v1.0.2' is gitlab tag
        ...
      },
    }
    ```

2. run command `npm install` to install this module to `node_modules`

# How to use

import this module and used.

eg:
```javascript
import Chat from 'lg-chat';

const chatroom = new Chat.LgChatRoom();
chatroom.getNewMessages();
```

# How to extend

create new file and extend 'chat' inside class like 'LgChatRoom'.

The define types reference [chat.d.ts](./types/index.d.ts).

eg:
```javascript
import Chat from 'lg-chat';

class HiWalletChatRoom extends Chat.LgChatRoom {
  // override base method for 'HiWallet'
  getNewMessages = () => {
    console.log('复写LgChatRoom的getNewMessages方法');
    return [];
  }

  // create new one for 'HiWallet'
  newFunction = () => {
    //...
  }
}

export {
  HiWalletChatRoom
}
```

# ESLint Check

Run failed now that cause by NodeJS 14.21.x not support 'Logical OR assignment' (||=).
NodeJs version must up than 16.x.

NodeJS 14.x that will throw error:
```
D:\projects\lg-chat\node_modules\@typescript-eslint\scope-manager\dist\referencer\ClassVisitor.js:123
        withMethodDecorators ||=
                             ^^^

SyntaxError: Unexpected token '||='
    at wrapSafe (internal/modules/cjs/loader.js:1001:16)
    at Module._compile (internal/modules/cjs/loader.js:1049:27)
    at Object.Module._extensions..js (internal/modules/cjs/loader.js:1114:10)
    at Module.load (internal/modules/cjs/loader.js:950:32)
    at Function.Module._load (internal/modules/cjs/loader.js:790:12)
    at Module.require (internal/modules/cjs/loader.js:974:19)
    at require (internal/modules/cjs/helpers.js:101:18)
    at Object.<anonymous> (D:\projects\lg-chat\node_modules\@typescript-eslint\scope-manager\dist\referencer\Referencer.js:20:24)
    at Module._compile (internal/modules/cjs/loader.js:1085:14)
    at Object.Module._extensions..js (internal/modules/cjs/loader.js:1114:10)
```
