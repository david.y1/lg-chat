import { LgChatRoomType, LgChatRoomSenderType, FunctionType } from '../../types';

class LgChatRoomSender implements LgChatRoomSenderType {
  functions: FunctionType[] = [];

  sendMessage = (chatroom: LgChatRoomType): Promise<{ messageId: number }> => {
    console.warn('Method not implemented.');
    const params = {
      chatId: chatroom.chatId,
      myId: chatroom.myId,
      msg: "this is test message."
    }
    return new Promise((resolve, reject) => {
      window.setTimeout(() => {
        const response = {
          code: 200,
          message: 'success',
          data: {
            messageId: 123
          }
        }
        resolve(response.data)
      }, 300)
    })
  }

  setupFunctions = (functions: FunctionType[]): void => {
    console.warn('Method not implemented.');
    this.functions = functions;
  }

  callFunction = (name: string, ...args: unknown[]): Promise<unknown> | unknown => {
    // console.warn('Method not implemented.');
    const fnObj = this.functions.find(fn => fn.name === name)
    const result = fnObj?.fn(...args)
    if (result && result instanceof Promise) {
      return Promise.resolve(result)
    } else {
      return result
    }
  }

  destroy = (): void => {
    console.warn('Method not implemented.');
    this.functions = [];
  }
}

export {
  LgChatRoomSender
}
