import { LgChatRoomType, LgChatRoomSenderType, FunctionType } from "../../types";
import { LgChatRoomSender } from './LgChatRoomSender';

/**
 * 聊天室类别
 */
export class LgChatRoom implements LgChatRoomType {
  chatId?: string | number | undefined;
  myId?: number;
  createTime?: number | undefined;
  sender?: LgChatRoomSenderType | undefined;
  functions: FunctionType[] = [];
  msgHandlers: Map<'add_history' | 'add_new', Set<(...args: unknown[]) => void>> = new Map();
  historyMessages: unknown[] = [];
  newMessages: unknown[] = [];

  init = (chatId: string | number, myId: number, historyMessages?: unknown[]): void => {
    console.warn('Method not implemented.');

    this._init(chatId, myId);

    if (historyMessages) {
      this.appendHistoryMessages(historyMessages);
    }
  }

  destroy = () => {
    console.warn('Method not implemented.');
    this.chatId = undefined;
    this.myId = undefined;
    this.createTime = undefined;
  }

  getAllMessage = ():unknown[] => {
    //console.warn('Method not implemented.');
    return ([] as unknown[]).concat(this.getHistoryMessages(), this.getNewMessages());
  }

  /**
   * 取得历史纪录的聊天讯息
   */
  getHistoryMessages = (): unknown[] => {
    // console.warn('Method not implemented.');
    return this.historyMessages;
  }

  appendHistoryMessages = (list: unknown[]): void => {
    //console.warn('Method not implemented.');
    this.historyMessages.push(...list);
    this.triggerMsgHandlers('add_history');
  }
  /**
   * 取得新的聊天讯息(非历史纪录)
   */
  getNewMessages = (): unknown[] => {
    // console.warn('Method not implemented.');
    return this.newMessages;
  }

  appendNewMessages = (list: unknown[]): void => {
    //console.warn('Method not implemented.');
    this.newMessages.push(...list);
    this.triggerMsgHandlers('add_new');
  }

  getStatus = (): { isBlock: boolean, info?: unknown } => {
    console.warn('Method not implemented.');
    return {
      isBlock: false,
    }
  }

  setupFunctions = (functions: FunctionType[]): void => {
    // console.warn('Method not implemented.');
    this.functions = functions;
  }

  callFunction = (name: string, ...args: unknown[]): unknown | Promise<unknown> => {
    // console.warn('Method not implemented.');
    const fnObj = this.functions.find(fn => fn.name === name);
    const result = fnObj?.fn(...args);

    if (result && result instanceof Promise) {
      return Promise.resolve(result);
    } else {
      return result;
    }
  }

  subscribe = (key: 'add_history' | 'add_new', fn: (...args: unknown[]) => void) => {
    if (!this.msgHandlers.has(key)) {
      this.msgHandlers.set(key, new Set());
    }

    const fns = this.msgHandlers.get(key);
    fns?.add(fn);
  }

  triggerMsgHandlers = (key: 'add_history' | 'add_new', ...args: unknown[]) => {
    const fns = this.msgHandlers.get(key);
    fns?.forEach(fn => fn(this, ...args));
  }

  unsubscribe = (key: 'add_history' | 'add_new', fn: (...args: unknown[]) => void) => {
    const set = this.msgHandlers.get(key);
    set?.delete(fn);
  }

  /** private method */
  _init = (chatId: string | number, myId: number) => {
    this.chatId = chatId;
    this.myId = myId
    this.createTime = Date.now();
    this.historyMessages = [];
    this.newMessages = [];
    this.sender = new LgChatRoomSender();
  }
}
